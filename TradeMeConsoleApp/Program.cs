﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeMeConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int weight = 0;
            int length = 0;
            int breadth = 0;
            int height = 0;
            string packagetype="";
            double cost=0.0;
            Console.WriteLine("Trade Me Application");
            Console.WriteLine("Package Type   Length   Breadth   Height   Cost");
            Console.WriteLine("Small          200mm   300mm    150mm     $5.00");
            Console.WriteLine("Medium         300mm   400mm    200mm     $7.50");
            Console.WriteLine("Large          400mm   600mm    250mm     $8.50");
            Console.WriteLine("Enter the Weight of the Package");
            weight = Int32.Parse(Console.ReadLine());
            if (weight > 25 )
            {
                Console.WriteLine("Excess Weight");
            }

            else
            {
                Console.WriteLine("Enter the Dimensions of the Package");
                Console.WriteLine("Enter the Length of the Package");
                length = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Enter the Breadth of the Package");
                breadth = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Enter the Height of the Package");
                height = Int32.Parse(Console.ReadLine());
                if (length < 200 && breadth < 300 && height < 150)
                {
                    cost = 5.00;
                    packagetype = "small";
                }
                else if (length < 300 && breadth < 400 && height < 200)
                {
                    cost = 7.50;
                    packagetype = "medium";
                }
                else if (length < 400 && breadth < 600 && height < 250)
                {
                    cost = 8.50;
                    packagetype = "large";
                }
                Console.WriteLine("Less Weight");
                Console.WriteLine("The Package Cost {0}",cost);
                Console.WriteLine("The Package Type {0}",packagetype);
            }
            Console.Read();
        }
    }
}
